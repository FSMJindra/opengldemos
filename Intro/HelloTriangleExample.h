#ifndef HELLOTRIANGLEEXAMPLE_H
#define HELLOTRIANGLEEXAMPLE_H
#include <OpenGLDemoBase.h>
#include <glm\glm.hpp>

class HelloTriangleExample :
	public GLCommon::OpenGLDemoBase
{
public:
	HelloTriangleExample();
	~HelloTriangleExample();

	void OnUpdate(double deltaTime) override;
private:
	// Data

	unsigned int mvpLocation;
	unsigned int programId;
	unsigned int vao;
	unsigned int vbo;
	glm::mat4 model;
	glm::mat4 proj;
	glm::mat4 view;
	std::vector<float> vertices;

	// Functions

	void Initialize();
	void SetInitialMatrices();
	void LoadVertexData();
	void CreateBuffers();
	void SetupShader();
	void SetVertexLayout();
};

#endif