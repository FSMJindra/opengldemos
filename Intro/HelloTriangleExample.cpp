#include "HelloTriangleExample.h"
#include <glm\glm.hpp>
#include <glm\gtx\transform.hpp>
#include <GL\glew.h>
#include <Utils.h>

HelloTriangleExample::HelloTriangleExample()
	: OpenGLDemoBase(800.0f, 600.0f, "Hello Triangle!")
{
	this->vao = 0;
	this->vbo = 0;
	this->Initialize();
}

HelloTriangleExample::~HelloTriangleExample()
{
	// Cleanup after ourselves.
	glDeleteBuffers(1, &this->vbo);
	glDeleteVertexArrays(1, &this->vao);
	glDeleteProgram(this->programId);
}

void HelloTriangleExample::Initialize()
{
	// Set matrices we need to transform vertices and display to screen.
	this->SetInitialMatrices();
	
	// Set the vertex data we want to draw.
	this->LoadVertexData();
	
	// Create necessary GL buffer objects to hold data, upload vertices to GPU.
	this->CreateBuffers();
	
	// Tell the GPU how to interpret our buffer.
	this->SetVertexLayout();
	
	// Create a shader program (small program that runs on the data we sent to the GPU.
	this->SetupShader();
}

void HelloTriangleExample::SetInitialMatrices()
{	
	// Set up our matrices;
	// A matrix that when we multiply our vertices with, will convert from model-space to "world" space.
	this->model = glm::mat4(1.0f);
	// A matrix that when multiplied by our "world"-space vertices, will translate them to "camera"-space.
	this->view = glm::lookAt(glm::vec3(0.0f, 0.0f, 5.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	// A matrix that when multiplied by our "camera"-space vertices, will translate them to clip space.  
	this->proj = glm::perspective(65.0f, float(this->GetXResolution()) / float(this->GetYResolution()), 0.01f, 100.0f);
	// The vertex shader will take clip-space coordinates and convert them to 'screen space (NDC)' and output
}

void HelloTriangleExample::LoadVertexData()
{
	GLCommon::GetTriangleGeometry(this->vertices);
}

void HelloTriangleExample::CreateBuffers()
{
	// Create Vertex Array Object (VAO) -- holds information regarding bound OpenGL state. 
	// Need at least one of these in modern OpenGL implementations.
	GLuint vertexArrayObject;
	glGenVertexArrays(1, &this->vao);
	glBindVertexArray(this->vao);

	// Create a Vertex Buffer Object (VBO) -- holds actual vertex data. These objects are
	// used to "upload" data to GPU memory. There are many ways of formatting the data inside this buffer, or across multiple buffers.
	// Layout is up to you! More on this later.
	glGenBuffers(1, &this->vbo);

	// Bind the new buffer as the active array buffer.
	// Several different buffer types. Array buffers, uniform buffers, element array buffers. More on this later.
	glBindBuffer(GL_ARRAY_BUFFER, this->vbo);

	if (vertices.size() > 0)
	{
		// Upload data to our currently bound array buffer. Calls to this will remove data that already exists in the buffer.
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * vertices.size(), &vertices[0], GL_STREAM_DRAW);
	}

	// If we want to free up CPU memory and don't need the data in "vertices" anymore we COULD
	// empty/delete the collection now.  The data is now residing in GPU controlled memory. However, we will leave it, since 
	// we will use it later to determine how many vertices we need to draw.
}

void HelloTriangleExample::SetupShader()
{
	this->programId = GLCommon::CreateShaderProgram("vertexShader.txt", "fragmentShader.txt");
	glUseProgram(programId);
	this->mvpLocation = glGetUniformLocation(programId, "mvp");
}

void HelloTriangleExample::SetVertexLayout()
{	
	// Since we supplied the GPU with a raw block of memory it has no idea what the hell is in it.
	// Here is where we tell it the memory layout. This is a terrible place to make mistakes.
	// Location Specified in Shader, Number of Elements, Type of Elements, Stride (Total width in bytes of one vertex), void pointer to the start of the data.
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));

	// See first couple lines of our shaders. Those layout = X correspond to the values in the next few statements.
	glEnableVertexAttribArray(0);	 // Positions
	glEnableVertexAttribArray(1);    // vertex colors
}

void HelloTriangleExample::OnUpdate(double deltaTime)
{
	float dTime = float(deltaTime);
	this->model = glm::rotate(this->model, 90.0f * float(dTime), glm::vec3(0.0f, 1.0f, 0.0f));
	glm::mat4 mvp = this->proj * this->view * this->model;
	// Send the matrix to the currently bound shader.
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, &mvp[0][0]);

	// Clear the screen! Get rid of color and depth information
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// I'll let you guess what this one does. Draw 3 vertices.
	glDrawArrays(GL_TRIANGLES, 0, 3);
}

