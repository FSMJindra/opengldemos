#ifndef MODELANDLIGHTINGDEMO_H
#define MODELANDLIGHTINGDEMO_H
#include <glm\glm.hpp>
#include <OpenGLDemoBase.h>

class ModelAndLightingDemo 
	: public GLCommon::OpenGLDemoBase
{
public:
	ModelAndLightingDemo();
	~ModelAndLightingDemo();

	void Initialize();
	void OnUpdate(double deltaTime) override;
private:
	unsigned int mvpLocation;
	glm::mat4 m;
	glm::mat4 p;
	glm::mat4 v;
};

#endif