#ifndef UTILS_H
#define UTILS_H
#ifdef GLCOMMON_EXPORTS
#define GLCOMMON_API __declspec(dllexport)
#else
#define GLCOMMON_API __declspec(dllimport)
#endif
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <ostream>
#include "Shader.h"
#include "OpenGLDemoBase.h"
namespace GLCommon
{
	GLCOMMON_API void RunProgram(OpenGLDemoBase* demo);
	GLCOMMON_API unsigned int CreateShaderProgram(const char* vertexShader, const char* fragmentShader);
	GLCOMMON_API void PrintGLVersion(std::ostream* stream);
	GLCOMMON_API void GetTriangleGeometry(std::vector<float>& outVertices);
}

#endif