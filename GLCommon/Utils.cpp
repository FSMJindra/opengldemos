#include "stdafx.h"
#include "Utils.h"
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <iostream>
#include <fstream>

namespace GLCommon
{
	GLCOMMON_API void RunProgram(OpenGLDemoBase * demo)
	{
		if (demo == nullptr) { throw std::exception("demo is null"); }
		demo->RenderLoop();
		delete demo;
		demo = nullptr;
	}

	GLCOMMON_API void GetTriangleGeometry(std::vector<float>& outVertices)
	{
		// X						Y							Z
		// R						G							B
		outVertices.push_back(-1.0f);	outVertices.push_back(-1.0f);	outVertices.push_back(0.0f);
		outVertices.push_back(1.0f);	outVertices.push_back(0.0f);	outVertices.push_back(0.0f);

		outVertices.push_back(1.0f);	outVertices.push_back(-1.0f);	outVertices.push_back(0.0f);
		outVertices.push_back(0.0f);	outVertices.push_back(1.0f);	outVertices.push_back(0.0f);
		
		outVertices.push_back(0.0f);	outVertices.push_back(1.0f);	outVertices.push_back(0.0f);
		outVertices.push_back(0.0f);	outVertices.push_back(0.0f);	outVertices.push_back(1.0f);
	}

	GLCOMMON_API GLuint CreateShaderProgram(const char* vertexShader, const char* fragmentShader)
	{
		Shader* s = new Shader(vertexShader, fragmentShader);
		return s->getId();
	}

	GLCOMMON_API void PrintGLVersion(std::ostream* stream)
	{
		if (stream == nullptr) { throw std::exception("null stream passed to PrintGLVersion"); }
		int maj, min;
		glGetIntegerv(GL_MAJOR_VERSION, &maj);
		glGetIntegerv(GL_MINOR_VERSION, &min);
		*stream << "OpenGL Context Version: " << maj << "." << min << std::endl;
		*stream << "---------------------------" << std::endl;
	}
}