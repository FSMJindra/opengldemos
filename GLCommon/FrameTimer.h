#ifndef FRAMETIMER_H
#define FRAMETIMER_H

#ifdef GLCOMMON_EXPORTS
#define GLCOMMON_API __declspec(dllexport)
#else
#define GLCOMMON_API __declspec(dllimport)
#endif

class GLCOMMON_API FrameTimer
{
public:
	FrameTimer();
	~FrameTimer();
	void StartFrame();
	void EndFrame();
	double inline GetDelta()
	{
		return this->m_deltaTime;
	}
private:
	double m_frameTime;
	double m_lastFrameTime;
	double m_deltaTime;
};

#endif