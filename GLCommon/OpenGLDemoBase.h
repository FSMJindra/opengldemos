#ifndef OPENGLDEMOBASE_H
#define OPENGLDEMOBASE_H

#ifdef GLCOMMON_EXPORTS
#define GLCOMMON_API __declspec(dllexport)
#else
#define GLCOMMON_API __declspec(dllimport)
#endif
#include <gl\glew.h>
#include <GLFW\glfw3.h>
#include <vector>

namespace GLCommon
{
	class GLCOMMON_API OpenGLDemoBase
	{
	public:
		OpenGLDemoBase(unsigned int resolutionX, unsigned int resolutionY, const char * windowTitle);
		~OpenGLDemoBase();
		void RenderLoop();

		virtual void OnUpdate(double deltaTime) = 0;
		virtual bool ShouldQuit();

	protected:
		void LaunchGLWindow();
		GLFWwindow* m_renderWindow;
		unsigned int GetXResolution();
		unsigned int GetYResolution();
	private:

		unsigned int m_windowResolutionX;
		unsigned int m_windowResolutionY;
		const char*  m_windowTitle;
		std::vector<unsigned int> vaos;
		std::vector<unsigned int> vbos;
	};
}
#endif