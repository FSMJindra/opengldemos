#include "stdafx.h"
#include "Shader.h"
#include <fstream>
#include <assert.h>
#include <vector>
#include <iostream>

Shader::Shader(std::string vertexFilePath, std::string fragmentFilePath)
{
	this->LoadShaders(vertexFilePath, fragmentFilePath);
}

Shader::~Shader()
{
}

unsigned int Shader::getId()
{
	return m_id;
}

void Shader::LoadShaders(const std::string& vertexFilePath, const std::string& fragmentFilePath)
{
	std::fstream fsV, fsF;
	fsV.open(vertexFilePath, std::ios::in);
	fsF.open(fragmentFilePath, std::ios::in);

	assert(fsV.is_open());
	assert(fsF.is_open());

	std::string vertexText   = this->ReadFromFileStream(fsV);
	std::string fragmentText = this->ReadFromFileStream(fsF);
	fsV.close();
	fsF.close();

	GLuint vertexShader   = this->CompileShader(GL_VERTEX_SHADER, vertexText);
	GLuint fragmentShader = this->CompileShader(GL_FRAGMENT_SHADER, fragmentText);
	GLuint program = glCreateProgram();
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);

	GLint infoLength = 0;
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLength);
	infoLength = infoLength < 1 ? 1 : infoLength;
	std::vector<char> ProgramErrorMsg(infoLength);
	glGetProgramInfoLog(program, infoLength, NULL, &ProgramErrorMsg[0]);
	for (unsigned int i = 0; i < infoLength; i++)
	{
		std::cout << ProgramErrorMsg[i];
	}
	std::cout << std::endl;
	this->m_id = program;
}

unsigned int Shader::CompileShader(GLenum shaderType, const std::string & shaderSource)
{
	GLuint shader = glCreateShader(shaderType);
	const char* sourcePtr = shaderSource.c_str();
	glShaderSource(shader, 1, &sourcePtr, NULL);
	glCompileShader(shader);

	GLint result = GL_FALSE;
	GLint infoLength;
	glGetShaderiv(shader, GL_LINK_STATUS, &result);
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLength);
	infoLength = infoLength < 1 ? 1 : infoLength;
	std::vector<char> ShaderErrorMessage(infoLength);
	glGetShaderInfoLog(shader, infoLength, NULL, &ShaderErrorMessage[0]);

	for (unsigned int i = 0; i < infoLength; i++)
	{
		std::cout << ShaderErrorMessage[i];
	}
	std::cout << std::endl;

	return shader;
}

std::string Shader::ReadFromFileStream(std::fstream & fs)
{
	std::string text;
	std::string line;
	while (std::getline(fs, line))
	{
		text += line + "\n";
	}
	return text;
}


