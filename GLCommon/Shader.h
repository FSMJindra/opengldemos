#pragma once
#ifdef GLCOMMON_EXPORTS
#define GLCOMMON_API __declspec(dllexport)
#else
#define GLCOMMON_API __declspec(dllimport)
#endif
#include <string>

class GLCOMMON_API Shader
{
public:
	Shader(std::string vertexFilePath, std::string fragmentFilePath);
	~Shader();
	unsigned int getId();
private:
	unsigned int m_id;
	void LoadShaders(const std::string& vertexFilePath, const std::string& fragmentFilePath);
	unsigned int CompileShader(GLenum shaderType, const std::string& shaderSource);
	std::string ReadFromFileStream(std::fstream& fs);
};

