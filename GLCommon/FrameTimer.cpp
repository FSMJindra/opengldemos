#include "stdafx.h"
#include "FrameTimer.h"


FrameTimer::FrameTimer()
{
	this->m_frameTime     = 0.0;
	this->m_lastFrameTime = 0.0;
	this->m_deltaTime     = 0.0;
}


FrameTimer::~FrameTimer()
{
}

void FrameTimer::StartFrame()
{
	this->m_frameTime = glfwGetTime();
	this->m_deltaTime = this->m_frameTime - this->m_lastFrameTime;
}

void FrameTimer::EndFrame()
{
	this->m_lastFrameTime = m_frameTime;
}
