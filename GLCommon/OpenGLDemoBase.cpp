#include "stdafx.h"
#include "OpenGLDemoBase.h"
#include "FrameTimer.h"
#include "Utils.h"
#include <iostream>

namespace GLCommon
{
	OpenGLDemoBase::OpenGLDemoBase(unsigned int resolutionX, unsigned int resolutionY, const char* windowTitle)
	{
		this->m_windowResolutionX = resolutionX;
		this->m_windowResolutionY = resolutionY;
		this->m_windowTitle       = windowTitle;
		this->LaunchGLWindow();
		GLCommon::PrintGLVersion(&std::cout);
	}

	OpenGLDemoBase::~OpenGLDemoBase()
	{
		glfwDestroyWindow(this->m_renderWindow);;
	}

	void OpenGLDemoBase::RenderLoop()
	{
		FrameTimer timer;
		while (!this->ShouldQuit())
		{
			timer.StartFrame();

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			this->OnUpdate(timer.GetDelta());
			glfwSwapBuffers(this->m_renderWindow);
			glfwPollEvents();
			
			timer.EndFrame();
		}
	}

	bool OpenGLDemoBase::ShouldQuit()
	{
		return glfwGetKey(this->m_renderWindow , GLFW_KEY_ESCAPE) == GLFW_PRESS || glfwWindowShouldClose(this->m_renderWindow);
	}

	unsigned int OpenGLDemoBase::GetXResolution()
	{
		return this->m_windowResolutionX;
	}

	unsigned int OpenGLDemoBase::GetYResolution()
	{
		return this->m_windowResolutionY;
	}

	void OpenGLDemoBase::LaunchGLWindow()
	{
		if (glfwInit() == GL_FALSE) { throw std::exception("Could not initialize GLFW."); }
		this->m_renderWindow = glfwCreateWindow(this->m_windowResolutionX, this->m_windowResolutionY, this->m_windowTitle, nullptr, nullptr);
		glfwMakeContextCurrent(this->m_renderWindow);
		glewExperimental = true;
		if (glewInit() != GLEW_OK) { throw std::exception("Glew failed initializaition"); }
	}
}